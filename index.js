// INTRODUCTION//

d3.select('h1').style('color','green')
.text('hello d3')

d3.select('body').append('p').text('first paragraph')
d3.select('body').append('p').text('second paragraph')
d3.select('body').append('p').text('third paragraph')

d3.selectAll('p').style('color','blue')

//Adding Elements//

var dataSet=[1,2,3,'hello',5]

d3.select('body')
    .selectAll('p')
    .data(dataSet)
    .enter()
    .append('p')
    .text('D3 is Awesome!!')
    .text(function(d) {return d});

// Creating Simple Bar-Chart//

var dataset=[80,120,60,150,60,240,180,90]
var dataset=[1,2,3,4,5]

var svgHeight="300" , svgWidth="500" ,barPadding=5
var barWidth=(svgWidth/dataset.length)

var svg = d3.select('svg')
          .attr("height",svgHeight)
          .attr("width",svgWidth)

var yScale = d3.scaleLinear()
            .domain([0,d3.max(dataset)])
            .range([0,svgHeight]);
     

var barChart=svg.selectAll('rect')  
             .data(dataset)
             .enter()
             .append("rect")
             .attr("y",function(d) { 
                return svgHeight-yScale(d)
             })
             .attr("width",barWidth-barPadding)
             .attr("height", function(d) {return yScale(d)})
             .attr("transform", function(d,i){
                 var translate=[barWidth*i,0];
                 return "translate("+translate+")"
             });
             
d3.select('svg').style("background-color","grey")
             
var text=svg.selectAll("text")
            .data(dataset)
            .enter()
            .append("text")
            .text(function(d) {return d})
            .attr("y",function(d,i) {return svgHeight-d-2})
            .attr("x",function(d,i) {return barWidth*i})
            .attr("fill","blue")

d3.select('h1').style("color","green")

// Creating X-axis And Y-axis//

var svgHeight=300,svgWidth=500

var svg = d3.select('svg')
    .attr("height",svgHeight)
    .attr("width",svgWidth)

var xScale = d3.scaleLinear()
    .domain([0,d3.max(dataset)])
    .range([0,svgWidth])


var yScale = d3.scaleLinear()
    .domain([0,d3.max(dataset)])
    .range([svgHeight,0])

var x_axis = d3.axisBottom().scale(xScale)
var y_axis = d3.axisLeft().scale(yScale)

svg.append("g")
    .attr("transform","translate(50,10)")
    .call(y_axis)

var xAxisTranslate = svgHeight - 20;
         
svg.append("g")
    .attr("transform", "translate(50, " + xAxisTranslate  +")")
    .call(x_axis);

// Creating SVG Elements//
var svgHeight=500,svgWidth=600

var svg= d3.select("svg")
    .attr("height",svgHeight)
    .attr("width",svgWidth)
    .attr("class","svg-container")


var line=svg.append("line")
    .attr("x1",100)
    .attr("x2",500)
    .attr("y1",50)
    .attr("y2",50)
    .attr("stroke","red")

var rect = svg.append("rect")
    .attr("x",200)
    .attr("y",100)
    .attr("width",200)
    .attr("height",100)
    .attr("fill","blue")

var circle=svg.append("circle")
    .attr("cx",300)
    .attr("cy",300)
    .attr("r",50)
    .attr("fill","green")


// Pie Chart//

var data = [
        {"platform": "Android", "percentage": 40.11}, 
        {"platform": "Windows", "percentage": 36.69},
        {"platform": "iOS", "percentage": 13.06}
    ];
    
    var svgWidth = 500, svgHeight = 300, radius =  Math.min(svgWidth, svgHeight) / 2;
    var svg = d3.select('svg')
        .attr("width", svgWidth)
        .attr("height", svgHeight);
    
    
    var g = svg.append("g")
        .attr("transform", "translate(" + radius + "," + radius + ")") ;
    
    var color = d3.scaleOrdinal(d3.schemeCategory10);
    
    var pie = d3.pie().value(function(d) { 
         return d.percentage; 
    });
    
    var path = d3.arc()
        .outerRadius(radius)
        .innerRadius(0);
     
    var arc = g.selectAll("arc")
        .data(pie(data))
        .enter()
        .append("g");
    
    arc.append("path")
        .attr("d", path)
        .attr("fill", function(d) { return color(d.data.percentage); });
            
    var label = d3.arc()
        .outerRadius(radius)
        .innerRadius(0);
                
    arc.append("text")
        .attr("transform", function(d) { 
            return "translate(" + label.centroid(d) + ")"; 
        })
        .attr("text-anchor", "middle")
        .text(function(d) { return d.data.platform+":"+d.data.percentage+"%"; });